import logging
FORMAT = '%(asctime)-5s [%(levelname)s] %(funcName)-8s %(message)s'

def init():
    logging.basicConfig(
        level = logging.DEBUG,
        format = FORMAT,
    )
